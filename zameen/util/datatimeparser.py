from datetime import datetime, timedelta

def parse_duration(duration_str):
    duration_map = {
        'year': 'years',
        'month': 'months',
        'week': 'weeks',
        'day': 'days',
        'hour': 'hours',
        'minute': 'minutes',
        'second': 'seconds'
    }
    duration_list = duration_str.split()
    if len(duration_list) == 3:
        value = int(duration_list[0])
        unit = duration_map[duration_list[1][:-1]]
        delta = timedelta(**{unit: value})
        return datetime.now() - delta
    else:
        return None

# Example usage
#duration_str = '2 weeks ago'
#duration_str = '6 years ago'
duration_str = '2 months ago'
#duration_str = '1 month ago'



parsed_date = parse_duration(duration_str)
if parsed_date is not None:
    print(parsed_date.strftime('%Y-%m-%d %H:%M:%S'))
else:
    print('Unable to parse duration')
