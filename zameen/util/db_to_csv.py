import pandas as pd
import pyodbc
from scrapy.utils.project import get_project_settings

# Set up the database connection
settings = get_project_settings()
conn = pyodbc.connect('DRIVER={};SERVER={};DATABASE={}'.format(
    settings['DRIVER'], settings['SERVER_NAME'], settings['DATABASE_NAME']))


# Query the table and load the data into a pandas dataframe
query = 'SELECT top 10000 * FROM ZameenProperty'
df = pd.read_sql(query, conn)


# Export the data to a CSV file
df.to_csv('../data/houses.csv', index=False)

