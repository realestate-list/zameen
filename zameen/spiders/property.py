from scrapy.spiders import CrawlSpider, Rule
from ..items import ZameenItem
from scrapy.loader import ItemLoader
from scrapy.linkextractors import LinkExtractor
from scrapy.utils.python import unique
from scrapy.utils.url import canonicalize_url
from scrapy.link import Link

class CusLinkExtractor(LinkExtractor):
    def extract_links(self, response):
        base_url = canonicalize_url(response.url)
        links = []
        for link in super().extract_links(response):
            link_url = canonicalize_url(link.url)
            if '/ur/' in link_url:
                link_url = link_url.replace('/ur/', '/')
                link = Link(url=link_url, text=link.text, fragment=link.fragment)
            links.append(link)
        return unique(links, key=lambda link: link.url)
class HouseSpider(CrawlSpider):
    name = "property"
    allowed_domains = ["zameen.com"]
    start_urls = ['https://www.zameen.com/all-cities/pakistan-1-9.html']

    rules = (
        Rule(LinkExtractor(allow='Houses_Property')),
        Rule(CusLinkExtractor(allow='Property'), callback='parse'),
    )

    def parse(self, response):
        type_xpath = "//span[@aria-label='Type']"
        area_xpath = "//span[@aria-label='Area']/span"
        price_xpath = "//span[@aria-label='Price']/div/text()[2]"
        purpose_xpath = "//span[@aria-label='Purpose']"
        location_xpath = "//span[@aria-label='Location']"
        bed_xpath = "//span[@aria-label='Beds']"
        bath_xpath = "//span[@aria-label='Baths']"
        created_xpath = "//span[@aria-label='Creation date']"
        description_xpath = "//div[@aria-label='Property description'] /div/span/text()[1]"
        property_id_xpath = "//div[@aria-label='Breadcrumb']/span[@aria-label='Link name']"
        city_xpath = "//div[@aria-label='Breadcrumb']/a[2]"
        town_xpath = "//div[@aria-label='Breadcrumb']/a[3]"
        phase_xpath = "//div[@aria-label='Breadcrumb']/a[4]"
        block_xpath = "//div[@aria-label='Breadcrumb']/a[5]"
        loader = ItemLoader(item=ZameenItem(), selector=response)
        loader.add_xpath("Type", type_xpath)
        loader.add_xpath("Area", area_xpath)
        loader.add_xpath("Price", price_xpath)
        loader.add_xpath("Purpose", purpose_xpath)
        loader.add_xpath("Location", location_xpath)
        loader.add_xpath("Bed", bed_xpath)
        loader.add_xpath("Bath", bath_xpath)
        loader.add_xpath("CreatedDate", created_xpath)
        loader.add_xpath("Description", description_xpath)
        loader.add_xpath("PropertyID", property_id_xpath)
        loader.add_xpath("City", city_xpath)
        loader.add_xpath("Town", town_xpath)
        loader.add_xpath("Phase", phase_xpath)
        loader.add_xpath("Block", block_xpath)
        loader.add_value("URL", response.url)
        yield loader.load_item()

