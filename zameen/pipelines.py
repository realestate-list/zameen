import pymssql
from itemadapter import ItemAdapter
import pypyodbc as odbc
class ZameenDatabasePipeline(object):
    def __init__(self, sqlServer, db):

        DRIVER = 'SQL Server'
        SERVER_NAME = sqlServer
        DATABASE_NAME = 'RealEstate'
        conn_string = f"""
            Driver={{{DRIVER}}};
            Server={SERVER_NAME};
            Database={DATABASE_NAME};
            Trust_Connection=yes;
        """
        self.sql = """
           INSERT INTO [RealEstate].[dbo].[ZameenProperty] (
                [Type],
                [Area],
                [Price],
                [Purpose],
                [Location],
                [Bed],
                [Bath],
                [CreatedDate],
                [Description],
                [PropertyID],
                [City],
                [Town],
                [Phase],
                [Block],
                [URL]
                )
                VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
           """
        self.conn = odbc.connect(conn_string)
        self.cursor = self.conn.cursor()

    def process_item(self, item, spider):
        try:
            adapter = ItemAdapter(item)
            values = (
                adapter.get('Type'),
                adapter.get('Area'),
                adapter.get('Price'),
                adapter.get('Purpose'),
                adapter.get('Location'),
                adapter.get('Bed'),
                adapter.get('Bath'),
                adapter.get('CreatedDate'),
                adapter.get('Description'),
                adapter.get('PropertyID'),
                adapter.get('City'),
                adapter.get('Town'),
                adapter.get('Phase'),
                adapter.get('Block'),
                adapter.get('URL'),
            )

            self.cursor.execute(self.sql, values)
            self.conn.commit()
        except (pymssql.Error):
            print("error")
        return item

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            sqlServer=crawler.settings.get('SERVER_NAME'),
            db=crawler.settings.get('RealEstate'),

        )
