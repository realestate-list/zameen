
BOT_NAME = 'zameen'

SPIDER_MODULES = ['zameen.spiders']
NEWSPIDER_MODULE = 'zameen.spiders'

DOWNLOADER_MIDDLEWARES = {
    'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,
    'scrapy_user_agents.middlewares.RandomUserAgentMiddleware': 400,
}

IMAGES_STORE = '.'
DRIVER = 'SQL Server'
SERVER_NAME = 'PSCA-LAPTOP-207\SQLEXPRESS'
DATABASE_NAME = 'RealEstate'


ITEM_PIPELINES = {
   'zameen.pipelines.ZameenDatabasePipeline': 300,
}
# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Configure maximum concurrent requests performed by Scrapy (default: 16)
CONCURRENT_REQUESTS = 10

FEED_EXPORT_ENCODING = 'utf-8'