import scrapy
from itemloaders.processors import MapCompose, TakeFirst
from w3lib.html import remove_tags, strip_html5_whitespace
from price_parser import Price
def get_price(raw_price):
    return Price.fromstring(raw_price).amount_text
def get_price_unit(raw_price):
    return Price.fromstring(raw_price).currency

def remove_house_key(raw):
    return raw.replace('Houses', '').replace('House', '')

class ZameenItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()

    Type = scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst()
    )
    Area = scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst()
    )

    Price = scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst()
    )

    Purpose = scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst()
    )
    Location = scrapy.Field(
        input_processor=MapCompose(remove_tags, strip_html5_whitespace),
        output_processor=TakeFirst()
    )
    Bed = scrapy.Field(
        input_processor=MapCompose(remove_tags, get_price),
        output_processor=TakeFirst()
    )
    Bath = scrapy.Field(
        input_processor=MapCompose(remove_tags, get_price),
        output_processor=TakeFirst()
    )
    CreatedDate = scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst()
    )
    Description = scrapy.Field(
        input_processor=MapCompose(remove_tags, strip_html5_whitespace),
        output_processor=TakeFirst()
    )
    PropertyID = scrapy.Field(
        input_processor=MapCompose(remove_tags, remove_house_key),
        output_processor=TakeFirst()
    )
    City = scrapy.Field(
        input_processor=MapCompose(remove_tags, remove_house_key),
        output_processor=TakeFirst()
    )
    Town = scrapy.Field(
        input_processor=MapCompose(remove_tags, remove_house_key),
        output_processor=TakeFirst()
    )
    Phase = scrapy.Field(
        input_processor=MapCompose(remove_tags, remove_house_key),
        output_processor=TakeFirst()
    )
    Block = scrapy.Field(
        input_processor=MapCompose(remove_tags, remove_house_key),
        output_processor=TakeFirst()
    )

    URL = scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst()
    )
